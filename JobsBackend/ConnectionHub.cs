﻿using JobsBackend.Data.Apis;
using JobsBackend.Data.Models;
using JobsBackend.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace JobsBackend
{
    public class ConnectionHub : Hub
    {
        private readonly JobApi _jobApi;
        private readonly JobProcessorApi _jobProcessorApi;
        private readonly ILogger<ConnectionHub> _logger;

        public ConnectionHub(JobApi jobApi, JobProcessorApi jobProcessorApi, ILogger<ConnectionHub> logger)
        {
            _jobApi = jobApi;
            _jobProcessorApi = jobProcessorApi;
            _logger = logger;
        }

        public async Task Finish(JobFinishBM model)
        {
            await _jobApi.SetStatusAsync(model.Key, JobStatus.Completed);

            await _jobProcessorApi.RemoveJobAsync(model.Processor);
        }

        public async Task Error(JobErrorResultBM model)
        {
            await _jobApi.SetStatusAsync(model.Key, JobStatus.Completed);

            await _jobProcessorApi.RemoveJobAsync(model.Processor);

            _logger.LogError($"{model.Key},{model.Processor}:{Environment.NewLine}{model.Message}");
        }
    }
}
