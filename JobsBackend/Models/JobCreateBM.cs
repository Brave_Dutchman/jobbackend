﻿using System.ComponentModel.DataAnnotations;

namespace JobsBackend.Models
{
    public class JobCreateBM
    {
        [Required]
        public string Key { get; set; }

        [Required]
        public int? TypeId { get; set; }

        public string Data { get; set; }
    }
}
