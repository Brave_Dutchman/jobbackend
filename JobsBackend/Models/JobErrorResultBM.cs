﻿using System.ComponentModel.DataAnnotations;

namespace JobsBackend.Models
{
    public class JobErrorResultBM
    {
        [Required]
        public string Processor { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
