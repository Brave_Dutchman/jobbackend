﻿using System.ComponentModel.DataAnnotations;

namespace JobsBackend.Models
{
    public class JobFinishBM
    {
        [Required]
        public string Processor { get; set; }

        [Required]
        public string Key { get; set; }
    }
}
