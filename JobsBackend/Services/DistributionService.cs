﻿using JobsBackend.Data;
using JobsBackend.Data.Apis;
using JobsBackend.Data.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace JobsBackend.Services
{
    public class DistributionService : BackgroundService
    {
        private readonly IJobTaskQueue _jobTaskQueue;
        private readonly IServiceProvider _serviceProvider;

        public DistributionService(IJobTaskQueue jobTaskQueue, IServiceProvider serviceProvider)
        {
            _jobTaskQueue = jobTaskQueue;
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var jobId = await _jobTaskQueue.DequeueAsync(stoppingToken).ConfigureAwait(false);
                if (!jobId.HasValue)
                {
                    continue;
                }

                using (var scope = _serviceProvider.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<JobDbContext>();

                    var job = await dbContext.Jobs.FindAsync(jobId);

                    var processor = await dbContext.JobProcessors
                        .Where(jp => jp.Status == JobProcessorStatus.Connected && jp.JobTypeId == job.JobTypeId && jp.ActiveJobId == null)
                        .FirstOrDefaultAsync();

                    if (processor == null)
                    {
                        //TODO requeue?
                    }

                    var api = scope.ServiceProvider.GetRequiredService<JobProcessorApi>();
                    var hubContext = scope.ServiceProvider.GetRequiredService<IHubContext<ConnectionHub>>();

                    await api.TakeJobAsync(processor, job);

                    await hubContext.Clients.Group(processor.GroupName).SendAsync("JobPickup", new JobPickupModel
                    {
                        Job = job.Key,
                        Processor = processor.Name,
                        JsonData = job.Data
                    });
                }
            }
        }
    }

    public class JobPickupModel
    {
        public string Job { get; set; }

        public string Processor { get; set; }

        public string JsonData { get; set; }
    }

    public interface IJobTaskQueue
    {
        void QueueBackgroundWorkItem(int jobId);

        Task<int?> DequeueAsync(CancellationToken cancellationToken);
    }

    public class JobTaskQueue : IJobTaskQueue
    {
        private ConcurrentQueue<int> _workItems = new ConcurrentQueue<int>();
        private SemaphoreSlim _signal = new SemaphoreSlim(0);

        public void QueueBackgroundWorkItem(int jobId)
        {
            _workItems.Enqueue(jobId);
            _signal.Release();
        }

        public async Task<int?> DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }
    }
}
