﻿using Microsoft.AspNetCore.Mvc;

namespace JobsBackend.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Content("Running", "text/plain");
        }
    }
}
