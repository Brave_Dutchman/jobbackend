﻿using JobsBackend.Data.Apis;
using JobsBackend.Data.Models;
using JobsBackend.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace JobsBackend.Controllers
{
    [ApiController]
    [Route("Jobs")]
    public class JobsController : Controller
    {
        private readonly JobApi _jobApi;

        public JobsController(JobApi jobApi)
        {
            _jobApi = jobApi;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(JobCreateBM model)
        {
            var response = await _jobApi.CreateAsync(model.Key, model.TypeId.Value, JobStatus.Initial, model.Data);
            if (response.Status == JobApiCreateStatus.Success)
            {
                return Ok();
            }

            return BadRequest(response.Status.ToString());
        }
    }
}
