﻿using JobsBackend.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace JobsBackend.Data
{
    public class JobDbContext : DbContext
    {
        public JobDbContext(DbContextOptions<JobDbContext> options)
            : base(options)
        { }

        public DbSet<Job> Jobs { get; set; }

        public DbSet<JobProcessor> JobProcessors { get; set; }

        public DbSet<JobType> JobTypes { get; set; }

        public DbSet<JobStatusLine> JobStatusLines { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }
}
