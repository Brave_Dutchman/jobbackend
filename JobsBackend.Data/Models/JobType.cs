﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JobsBackend.Data.Models
{
    public class JobType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    internal class JobTypeConfiguration : IEntityTypeConfiguration<JobType>
    {
        public void Configure(EntityTypeBuilder<JobType> builder)
        {
            builder.Property(i => i.Name).HasMaxLength(100).IsRequired();
        }
    }
}
