﻿namespace JobsBackend.Data.Models
{
    public enum JobStatus
    {
        Initial = 0,
        Processing = 40,
        Completed = 60,
        Error = 100
    }
}
