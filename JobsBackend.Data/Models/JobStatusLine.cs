﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace JobsBackend.Data.Models
{
    public class JobStatusLine
    {
        internal JobStatusLine()
        { }

        public int Id { get; set; }

        public JobStatus Status { get; set; }

        public bool IsCurrent { get; internal set; }

        public DateTime CreatedOn { get; set; }

        public virtual Job Job { get; set; }
    }

    internal class JobStatusLineConfiguration : IEntityTypeConfiguration<JobStatusLine>
    {
        public void Configure(EntityTypeBuilder<JobStatusLine> builder)
        {
            builder.HasOne(jsl => jsl.Job).WithMany(jsl => jsl.Statuses).OnDelete(DeleteBehavior.Restrict).IsRequired(false);

            builder.Property(jsl => jsl.IsCurrent).IsRequired();

            builder.Property(jsl => jsl.CreatedOn).IsRequired();
        }
    }
}
