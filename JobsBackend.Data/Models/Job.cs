﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace JobsBackend.Data.Models
{
    public class Job
    {
        internal Job()
        { }

        public int Id { get; set; }

        public string Key { get; set; }

        public string Data { get; set; }

        public int JobTypeId { get; set; }
        public virtual JobType Type { get; internal set; }

        public int CurrentStatusId { get; set; }
        public virtual JobStatusLine CurrentStatus { get; internal set; }

        public virtual ICollection<JobStatusLine> Statuses { get; set; }
    }

    internal class JobConfiguration : IEntityTypeConfiguration<Job>
    {
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.Property(i => i.Data).HasMaxLength(100).IsRequired();

            builder.Property(o => o.Key).IsRequired();

            builder.HasIndex(o => o.Key).IsUnique();

            builder.HasOne(o => o.CurrentStatus).WithMany().IsRequired();

            builder.HasOne(o => o.Type).WithMany().IsRequired();
        }
    }
}
