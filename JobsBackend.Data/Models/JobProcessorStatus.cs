﻿namespace JobsBackend.Data.Models
{
    public enum JobProcessorStatus
    {
        Connected = 0,
        Disconnected = 20
    }
}
