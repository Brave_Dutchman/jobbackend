﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JobsBackend.Data.Models
{
    public class JobProcessor
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string GroupName { get; set; }

        public int? ActiveJobId { get; internal set; }

        public virtual Job ActiveJob { get; internal set; }

        public int JobTypeId { get; internal set; }

        public virtual JobType JobType { get; internal set; }

        public JobProcessorStatus Status { get; set; }
    }

    internal class JobProcessoronfiguration : IEntityTypeConfiguration<JobProcessor>
    {
        public void Configure(EntityTypeBuilder<JobProcessor> builder)
        {
            builder.Property(i => i.Name).HasMaxLength(100).IsRequired();

            builder.HasIndex(i => i.Name).IsUnique();

            builder.Property(i => i.GroupName).HasMaxLength(100).IsRequired();

            builder.HasIndex(i => i.GroupName).IsUnique();

            builder.Property(i => i.Status).IsRequired();

            builder.Property(i => i.ActiveJobId).IsRequired(false);
        }
    }
}
