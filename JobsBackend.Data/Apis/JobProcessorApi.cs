﻿using JobsBackend.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace JobsBackend.Data.Apis
{
    public class JobProcessorApi
    {
        private readonly JobDbContext _dbContext;
        private readonly JobApi _jobApi;

        public JobProcessorApi(JobDbContext dbContext, JobApi jobApi)
        {
            _dbContext = dbContext;
            _jobApi = jobApi;
        }

        public async Task RemoveJobAsync(string processorName)
        {
            var processor = await _dbContext.JobProcessors.SingleAsync(p => p.Name == processorName);

            processor.ActiveJobId = null;

            await _dbContext.SaveChangesAsync();
        }

        public async Task TakeJobAsync(JobProcessor jobProcessor, Job job)
        {
            jobProcessor.ActiveJobId = job.Id;

            await _jobApi.SetStatusAsync(job.Id, JobStatus.Processing);
        }
    }
}
