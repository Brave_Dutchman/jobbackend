﻿using JobsBackend.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JobsBackend.Data.Apis
{
    public class JobApi
    {
        private readonly JobDbContext _dbContext;

        public JobApi(JobDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Response<JobApiCreateStatus, Job>> CreateAsync(string key, int typeId, string data)
        {
            return await CreateAsync(key, typeId, JobStatus.Initial, data);
        }

        public async Task<Response<JobApiCreateStatus, Job>> CreateAsync(string key, int typeId, JobStatus status, string data)
        {
            var jobType = _dbContext.JobTypes.SingleOrDefault(o => o.Id == typeId);
            if (jobType == null)
            {
                return Response.Create(JobApiCreateStatus.Error_InvalidType, default(Job));
            }

            var jobStatus = new JobStatusLine
            {
                Status = status,
                IsCurrent = true,
                CreatedOn = DateTime.Now
            };

            var job = new Job()
            {
                Key = key,
                JobTypeId = typeId,
                Data = data ?? "{}",
                CurrentStatus = jobStatus,
            };

            _dbContext.Add(job);
            await _dbContext.SaveChangesAsync();

            jobStatus.Job = job;
            await _dbContext.SaveChangesAsync();

            return Response.Create(JobApiCreateStatus.Success, job);
        }

        public async Task SetStatusAsync(int jobId, JobStatus status)
        {
            var job = await _dbContext.Jobs.Where(j => j.Id == jobId)
                .Include(j => j.CurrentStatus)
                .SingleAsync();

            await _setStatusAsync(job, status);
        }

        public async Task SetStatusAsync(string jobKey, JobStatus status)
        {
            var job = await _dbContext.Jobs.Where(j => j.Key == jobKey)
                .Include(j => j.CurrentStatus)
                .SingleAsync();

            await _setStatusAsync(job, status);
        }

        private async Task _setStatusAsync(Job job, JobStatus status)
        {
            var oldStatus = job.CurrentStatus;
            if (oldStatus != null)
            {
                if (oldStatus.Status == status)
                {
                    return;
                }

                oldStatus.IsCurrent = false;
            }

            job.CurrentStatus = new JobStatusLine
            {
                Status = status,
                Job = job,
                IsCurrent = true,
                CreatedOn = DateTime.Now
            };

            await _dbContext.SaveChangesAsync();
        }
    }

    public enum JobApiCreateStatus
    {
        Success = 0,
        Error_InvalidType = 10
    }
}
