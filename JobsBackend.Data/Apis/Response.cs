﻿namespace JobsBackend.Data.Apis
{
    public class Response<TStatus, TResult>
    {
        internal Response(TStatus status, TResult result)
        {
            Status = status;
            Result = result;
        }

        public TStatus Status { get; }

        public TResult Result { get; }
    }

    public static class Response
    {
        public static Response<TStatus, TResult> Create<TStatus, TResult>(TStatus status, TResult result)
        {
            return new Response<TStatus, TResult>(status, result);
        }
    }
}
